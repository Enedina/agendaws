package com.example.agendatarea3;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

   //final ArrayList<Contacto> contactos= new ArrayList<Contacto>();
    private EditText edtNombre;
    private EditText edtTelefono;
    private EditText edtTelefono2;
    private EditText edtDireccion;
    private EditText edtNotas;
    private CheckBox cbxFavorito;
    private Contacto savedContact;
    private AgendaContactos db;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre= (EditText) findViewById(R.id.txtNombre);
        edtTelefono= (EditText) findViewById(R.id.txtTel1);
        edtTelefono2= (EditText) findViewById(R.id.txtTel2);
        edtDireccion = (EditText) findViewById(R.id.txtDomicilio);
        edtNotas =(EditText) findViewById(R.id.txtNota);
        cbxFavorito = (CheckBox)findViewById(R.id.chkFavorito);
        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);

        db = new AgendaContactos(MainActivity.this);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edtNombre.getText().toString().equals("")||
                        edtDireccion.getText().toString().equals("")||
                        edtTelefono.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, R.string.msgerror,
                            Toast.LENGTH_SHORT).show();
                }else{
                    Contacto nContacto = new Contacto();

                    nContacto.setNombre(edtNombre.getText().toString());
                    nContacto.setDomicilio(edtDireccion.getText().toString());
                    nContacto.setNotas(edtNotas.getText().toString());
                    nContacto.setTelefono1(edtTelefono.getText().toString());
                    nContacto.setTelefono2(edtTelefono2.getText().toString());
                    nContacto.setFavorito(cbxFavorito.isChecked() ? 1 : 0);

                    if(cbxFavorito.isChecked()) {
                        nContacto.setFavorito(1);
                    }

                    else{
                        nContacto.setFavorito(0);
                    }

                    db.openDataBase();

                    if (savedContact==null){
                        long idx = db.insertarContacto(nContacto);
                        Toast.makeText(MainActivity.this,"Se agregó contacto con ID: " +idx,Toast.LENGTH_SHORT).show();
                    }

                    else{
                        db.UpdateContacto(nContacto,id);
                        Toast.makeText(MainActivity.this,"Se actualizo el Registro: " +id,Toast.LENGTH_SHORT).show();
                    }

                    db.cerrar();

                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiar();
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =  new Intent(MainActivity.this, ListActivity.class);
                startActivityForResult(i, 0);
            }
        });
    }

    protected void onActivityResult(int requestcode, int resultCode, Intent data){
        if (Activity.RESULT_OK == resultCode){
            Contacto contacto = (Contacto) data.getSerializableExtra("contacto");
            savedContact = contacto;
            id = (int) contacto.get_ID();
            edtNombre.setText(contacto.getNombre());
            edtDireccion.setText(contacto.getDomicilio());
            edtTelefono.setText(contacto.getTelefono1());
            edtTelefono2.setText(contacto.getTelefono2());
            edtNotas.setText(contacto.getNotas());
            if (contacto.getFavorito()>0){
                cbxFavorito.setChecked(true);
            }
            else {
                limpiar();
            }
        }
    }

    public void limpiar()
    {
        edtNombre.setText("");
        edtTelefono.setText("");
        edtTelefono2.setText("");
        edtDireccion.setText("");
        edtNotas.setText("");
        cbxFavorito.setChecked(false);

    }
}
                                                                                                                                                                                                                                                                                            